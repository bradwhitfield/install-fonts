# README #

A PowerShell script that will install all fonts in a directory and all it's sub-directories.

The script was created for my wife. We just got her a new Windows 8.1 machine for her to do all of her design/technical writing work, and she had collected a large number of fonts over time. Some of these fonts are in OFT format, others are in TTF format, and some are zipped up. Well since we will soon be getting her a better SSD, the laptop might end up reloaded again. Beside that, I'm sure she will go on another spree where she downloads a ton of fonts and needs to install them, so this script work for that as well.

Similar to this, I wrote a script to [install Photoshop brushes](https://bitbucket.org/bradwhitfield/install-brushes) as well.

##Note!##

This comes with no warranty! Read the script before you blindly trust it. I really can't think of how it would mangle your machine, unless you had a bad font, but you still need to be responsible with any script you find on the web.

There is a prompt at the end of the script that ask about deleting a folder. If this folder is TempFontsDir, it should be safe to click "Yes". This is just the directory that any extracted fonts were put before being installed. I really can't see any time when it wouldn't be that folder, but I would rather be cautious and let the user choose to delete it.

### Script Info ###

* Tested on Windows 10, 8.1, and 8
* Tested with PowerShell version 3, 4, and 5
* Based on info from a [Windows IT Pro article](http://windowsitpro.com/scripting/trick-installing-fonts-vbscript-or-powershell-script)

### How do I run this? ###

1. Download the .ps1 file.
2. From the Start Screen, search for "PowerShell"
3. Right click PowerShell click "Run as Administrator"
4. Enter the command "Set-ExecutionPolicy RemoteSigned"
5. Enter "Y" at the prompt
6. Change directory to the folder containing the script (probably "cd Downloads")
7. Enter ".\InstallFonts.ps1"
8. Now enter "Add-Fonts -InstallFrom D:\Fonts\" replacing the 'D:\Fonts\' location with your fonts folder
9. When you get prompted to confirm deleting a directory, click "Yes" if it says "TempFontsDir" and if you didn't create one yourself

### TODO ###

* Make this a module
* Sign it
* Make instructions to install module
* Make a switch for automatically overwrite files
* Add more comments
* Add Verbose Output

### Feedback ###

Easiest way to submit feedback for this project is probably just to message me through BitBucket, or you could submit an issue. This is just a small script, so there won't be any real elaborate forms of communication for this project.