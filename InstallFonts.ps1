<#
.Synopsis
   Installs all fonts from a specified directory.
.DESCRIPTION
   Installs all fonts from the current, or specified, directory.
   The script copies the font to the appropriate folder and registers the Font in the registry.
   Works with TTF and OTF currently.
.EXAMPLE
   PS D:\Fonts> Add-Fonts
.EXAMPLE
   PS D:\> Add-Fonts -InstallFrom ".\Fonts"
#>
function Add-Fonts
{
    [CmdletBinding()]
    Param
    (
        # The location of the fonts to install. Default is current directory.
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $InstallFrom = "."
    )

    Begin
    {
        #Change to the directory where the fonts are said to be located
        Set-Location $InstallFrom

        $FONTS = 0x14 #Windows' representation for the fonts directory
        $TEMPFONTSDIR = "TempFontDir\" #The directory zipped fonts will be extracted to
        $ShellObject = New-Object -ComObject Shell.Application #A Windows shell

        #Get all zip files
        $ZippedFonts = Get-ChildItem -Recurse -Force -Include *.zip

        #Create a temporary directory to unzip fonts to.
        New-Item -ItemType Directory -Path $TEMPFONTSDIR
    }
    Process
    {
        foreach ($zip in $ZippedFonts)
        {
            #Extract fonts from zip files and copy them to temporary directory
            $ZipFile = $ShellObject.NameSpace($zip.FullName)
            foreach($file in $ZipFile.items())
            {
                if ($file.Name.EndsWith(".ttf") -or $file.Name.EndsWith(".oft"))
                {
                    $FolderObject = $ShellObject.NameSpace((Get-Location).Path + "\" + $TEMPFONTSDIR)
                    $FolderObject.CopyHere($file)
                }
            }
        }

        #Get all fonts in all the current directory and all subdirectories
        $FontsToInstall = Get-ChildItem -Recurse -Force -Include *.ttf,*.oft

        #Install the fonts to the proper directory
        foreach ($font in $FontsToInstall)
        {
            #Avoid Mac folder formatting
            if (-not($font.Name.StartsWith("._")))
            {
                $FolderObject = $ShellObject.NameSpace($FONTS)
                $FolderObject.CopyHere($font.FullName)
            }

        }
    }
    End
    {
        #Delete the temporary directory created where the zip fonts were held
        Remove-Item -Recurse -Confirm -Path ((Get-Location).Path + "\" + $TEMPFONTSDIR)
    }
}